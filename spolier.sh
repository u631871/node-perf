# bash spoilers.sh
perf record -e cycles:u -g -- node --perf-basic-prof app.js
perf script | egrep -v "( __libc_start| LazyCompile | v8::internal::| Builtin:| Stub:| LoadIC:|\[unknown\]| LoadPolymorphicIC:)" | sed 's/ LazyCompile:[*~]\?/ /' > perfs.out
stackvis perf < perfs.out > flamegraph.htm

rm isolate*
rm perf*

perf record -e cycles:u -g -- node --perf-basic-prof app.queued.js
perf script | egrep -v "( __libc_start| LazyCompile | v8::internal::| Builtin:| Stub:| LoadIC:|\[unknown\]| LoadPolymorphicIC:)" | sed 's/ LazyCompile:[*~]\?/ /' > perfs.out
stackvis perf < perfs.out > flamegraph-queued.htm

rm isolate*
rm perf*