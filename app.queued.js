
const async = require('async');

function runner(num, callback) {
  setTimeout(callback, 1000 * num);
}

function run() {

  const sample = 100;
  const concurrency = 5;

  const q = async.queue((task, cb) => {
    runner(task.num, () => {
      console.log(`${task.index} processed`);
      cb();
    });
  }, concurrency);

  for (let i = 0; i < sample; i++) {
    q.push({ num: Math.random(), index: i });
  }
  
  q.drain(() => console.log(`${sample} jobs processed`));

}


run();