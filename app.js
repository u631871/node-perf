
function runner(num, callback) {
  setTimeout(callback, 1000 * num);
}

function run() {

  const sample = 100;

  for (let i = 0; i < sample; i++) {
    runner(Math.random(), () => {
      console.log(`${i} processed`);
    });
  }

}


run();